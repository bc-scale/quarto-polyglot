FROM debian:stable

RUN apt -y update && \
    apt -y upgrade && \
    apt -y install curl

# Install Quarto
RUN curl -LO https://github.com/quarto-dev/quarto-cli/releases/download/v1.2.237/quarto-1.2.237-linux-amd64.deb && \
    apt install -y gdebi-core && \
    gdebi -n quarto-1.2.237-linux-amd64.deb

# Install R
RUN apt -y install r-base && \
    R -e "install.packages(c('rmarkdown', 'renv'))"

# Install Jupyter
RUN apt -y install python3-pip && python3 -m pip install jupyter

# Install Julia
RUN pip install jill && \
    jill install --confirm && \
    julia -e 'import Pkg; Pkg.add(["IJulia", "Revise"])'

# Install TinyTeX
RUN quarto install tinytex

# Check Quarto
RUN quarto check
